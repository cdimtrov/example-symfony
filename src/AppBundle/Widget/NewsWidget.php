<?php

namespace AppBundle\Widget;

/**
 * Description of NewsWidget
 *
 * @author chavdar
 */
class NewsWidget extends Widget {

    public function __construct() {
        parent::__construct();
    }

    public function run() {
        $twig = $this->container->get('twig');
        $em = $this->container->get('doctrine')->getManager();
        $query = $em->createQuery(
                'SELECT News
                FROM AppBundle:News News
                Order By News.id
                '
        );

        $news = $query->setMaxResults(3)->getResult();

        return $twig->render('widgets/news.html.twig', array(
                    'news' => $news
        ));
    }

}
