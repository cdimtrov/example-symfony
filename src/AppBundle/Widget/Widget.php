<?php

namespace AppBundle\Widget;

use Symfony\Component\DependencyInjection\ContainerInterface;

class Widget {

    public $twig;
    public $currentLocale;
    protected $container;

    public function __construct(ContainerInterface $container = null) {
        $this->container = $container;
    }

    public static function widget(ContainerInterface $container = null, array $params = array()) {
        $class = get_called_class();
        $widget = new $class();
        $widget->setContainer($container);
        $widget->setCurrentLocale($container);

        return $widget->run();
    }

    public function setContainer($di) {
        $this->container = $di;
    }

    public function setCurrentLocale($container) {
        $request = $container->get('request_stack');
        $locale = $request->getCurrentRequest();
        $this->currentLocale = $locale->getLocale();
    }

}
