<?php

namespace AppBundle\Widget;

/**
 * Description of SliderWidget
 *
 * @author chavdar
 */
class SliderWidget extends Widget {

    public function __construct() {
        parent::__construct();
    }

    public function run() {
        $twig = $this->container->get('twig');
        $em = $this->container->get('doctrine')->getManager();
        $query = $em->createQuery(
                'SELECT Slider
                 FROM AppBundle:Slider Slider
                 WHERE Slider.isActive > 0
                 Order By Slider.position
                 '
        );
        $sliders = $query->getResult();

        return $twig->render('widgets/slider.html.twig', array(
            'sliders' => $sliders,
            'currentLocale' => $this->currentLocale
        ));
    }

}
