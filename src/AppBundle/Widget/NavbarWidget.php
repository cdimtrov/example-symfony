<?php

namespace AppBundle\Widget;

/**
 * Description of MenuWidget
 *
 * @author chavdar
 */
class NavbarWidget extends Widget {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function run() {
        $twig = $this->container->get('twig');
        return $twig->render('widgets/navbar.html.twig', array(
        ));
    }
    
    
    
    
    
}
