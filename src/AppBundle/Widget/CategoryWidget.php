<?php

namespace AppBundle\Widget;

/**
 * Description of CategoryWidget
 *
 * @author chavdar
 */
class CategoryWidget extends Widget {

    public function __construct() {
        parent::__construct();
    }

    public function run() {
        $twig = $this->container->get('twig');
        $em = $this->container->get('doctrine')->getManager();
        $query = $em->createQuery(
                'SELECT Category
                FROM AppBundle:Category Category

                WHERE Category.parent IS NULL
                Order By Category.id
                '
        );

        $categories = $query->getResult();


        return $twig->render('widgets/category.html.twig', array(
                    'categories' => $categories
        ));
    }

}
