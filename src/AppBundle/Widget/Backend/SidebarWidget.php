<?php

namespace AppBundle\Widget\Backend;

use AppBundle\Widget\Widget;
/**
 * Description of SidebarWidget
 *
 * @author chavdar
 */
class SidebarWidget extends Widget {

    public function __construct() {
        parent::__construct();
    }

    public function run() {
        $twig = $this->container->get('twig');
        return $twig->render('widgets/backend/sidebar.html.twig', array(
            'testing' => 'тестване'
        ));
    }





}
