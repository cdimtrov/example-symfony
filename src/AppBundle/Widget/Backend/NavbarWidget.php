<?php

namespace AppBundle\Widget\Backend;

use AppBundle\Widget\Widget;

/**
 * Description of MenuWidget
 *
 * @author chavdar
 */
class NavbarWidget extends Widget {

    public function __construct() {
        parent::__construct();
    }

    public function run() {
        $twig = $this->container->get('twig');
        return $twig->render('widgets/backend/navbar.html.twig', array(
        ));
    }





}
