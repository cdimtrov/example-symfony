<?php

namespace AppBundle\Twig;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Description of Widget
 *
 * @author chavdar
 */
class Widget extends \Twig_Extension {

    public function __construct(ContainerInterface $container = null) {
        $this->container = $container;
    }

    public function getFunctions() {
        $function = new \Twig_SimpleFunction('widget', function ($name, $arguments = array()) {
            $class = trim(ucfirst($name)) . 'Widget';
            $params = $arguments;
            $request = $this->container->get('request_stack')->getCurrentRequest();
            $url = $request->getRequestUri();
            if (strpos($url, 'admin') !== false) {
                $widgetClass = "\\AppBundle\\Widget\\Backend\\$class";
            } else {
                $widgetClass = "\\AppBundle\\Widget\\$class";
            }
            return $widgetClass::widget($this->container, $params);
        }, array('is_safe' => array('html')));

        return array(
            $function
        );
    }


    public function getName() {
        return 'widget';
    }

}
