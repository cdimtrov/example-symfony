<?php

namespace AppBundle\Form\Extension;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Sonata\MediaBundle\Form\Type\MediaType;

class MediaTypeExtension extends AbstractTypeExtension
{
    /**
     * {@inheritdoc}
     */

    /**
     * @param Pool   $pool
     * @param string $class
     */
    public function __construct(Pool $pool, $class) {
        $this->pool = $pool;
        $this->class = $class;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
//        $builder->addModelTransformer(new ProviderDataTransformer($this->pool, $this->class, array(
//            'provider' => $options['provider'],
//            'context' => $options['context'],
//            'empty_on_new' => $options['empty_on_new'],
//            'new_on_update' => $options['new_on_update'],
//        )));
//
//        $builder->addEventListener(FormEvents::BIND, function (FormEvent $event) {
//            if ($event->getForm()->get('unlink')->getData()) {
//                $event->setData(null);
//            }
//        });
        var_dump($options);
        exit();
        $this->pool->getProvider($options['provider'])->buildMediaType($builder);

//        $builder->add('unlink', 'checkbox', array(
//            'label' => 'изтрий',
//            'mapped' => false,
//            'data' => false,
//            'required' => false,
//        ));
    }

//    /**
//     * {@inheritdoc}
//     */
//    public function buildView(FormView $view, FormInterface $form, array $options) {
//        $view->vars['provider'] = $options['provider'];
//        $view->vars['context'] = $options['context'];
//    }

//    /**
//     * {@inheritdoc}
//     */
//    public function setDefaultOptions(OptionsResolverInterface $resolver) {
//        $resolver->setDefaults(array(
//            'data_class' => $this->class,
//            'provider' => null,
//            'context' => null,
//            'empty_on_new' => true,
//            'new_on_update' => true,
//        ));
//    }

    /**
     * {@inheritdoc}
     */
    public function getExtendedType() {
        return MediaTypse::class;
    }

}
