<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Trsteel\CkeditorBundle\Form\Type\CkeditorType;
use Sonata\MediaBundle\Form\Type\MediaType;


/**
 * Description of ProductType
 *
 * @author chavdar
 */
class ProductType extends AbstractType  {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('translations', TranslationsType::class, array(
                    'fields' => array(
                        'title' => array(
                            'field_type' => TextType::class,
                            'label' => 'Име на продукт',
                            'attr' => array(
                                'class' => 'form-control'
                            ),
                        ),
                        'description' => array(
                            'field_type' => CkeditorType::class,
                            'label' => 'Описание',
                            'attr' => array(
                                'class' => 'form-control',
                                'rows' => '10'
                            ),
                        )
                    )
                ))
                ->add('image', MediaType::class, array(
                    'provider' => 'sonata.media.provider.image',
                    'context' => 'product',
                ))
                ->add('category', EntityType::class, array(
                    'class' => 'AppBundle:Category',
                    'choice_label' => 'translations[bg].getName',
                    'label' => 'категория',
                    'placeholder' => 'Изберете родителска категория ако е нужно',
                    'attr' => array(
                        'class' => 'form-group'
                    )
                ))

                ->add('save', SubmitType::class, array(
                    'label' => 'Запази',
                    'attr' => array(
                        'class' => 'btn btn-primary'
        )));
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Product',
        ));
    }

}
