<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Trsteel\CkeditorBundle\Form\Type\CkeditorType;
use Sonata\MediaBundle\Form\Type\MediaType;

/**
 * Description of News
 *
 * @author chavdar
 */
class NewsType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('translations', TranslationsType::class, array(
            'label' => ' ',
            'fields' => array(
                'title' => array(
                    'field_type' => TextType::class,
                    'label' => 'Име на новина',
                    'attr' => array(
                        'class' => 'form-control'
                    ),
                ),
                'description' => array(
                    'field_type' => CkeditorType::class,
                    'label' => 'Описание',
                    'attr' => array(
                        'class' => 'form-control',
                        'rows' => '4'
                    ),
                )
            )
        ));
        $builder->add('image', MediaType::class, array(
            'provider' => 'sonata.media.provider.image',
            'context' => 'news',
        ));

        $builder->add('save', SubmitType::class, array(
            'label' => 'Запази',
            'attr' => array(
                'class' => 'btn btn-lg btn-primary'
        )));
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\News',
        ));
    }

}
