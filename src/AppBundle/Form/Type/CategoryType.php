<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Doctrine\ORM\EntityRepository;
use Trsteel\CkeditorBundle\Form\Type\CkeditorType;
use Sonata\MediaBundle\Form\Type\MediaType;

class CategoryType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('translations', TranslationsType::class, array(
                    'label' => ' ',
                    'fields' => array(
                        'name' => array(
                            'field_type' => TextType::class,
                            'label' => 'Име на категория',
                            'attr' => array(
                                'class' => 'form-control'
                            )
                        ),
                        'description' => array(
                            'field_type' => CkeditorType::class,
                            'label' => 'Описание',
                            'attr' => array(
                                'maxlength' => 250,
                                'class' => 'form-control',
                                'rows' => '10'
                            )
                        )
                    )
                        )
                )
                ->add('parent', EntityType::class, array(
                    'class' => 'AppBundle:Category',
                    'choice_label' => 'translations[bg].getName',
                    'label' => 'Родителска категория',
                    'placeholder' => 'Изберете родителска категория ако е нужно',
                    'attr' => array(
                        'class' => 'form-group'
                    )
                ))
                ->add('image', MediaType::class, array(
                    'provider' => 'sonata.media.provider.image',
                    'context' => 'category',
                ))
                ->add('save', SubmitType::class, array(
                    'label' => 'Запази',
                    'attr' => array(
                        'class' => 'btn btn-lg btn-primary'
                    )
                        )
        );
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Category',
        ));
    }

}
