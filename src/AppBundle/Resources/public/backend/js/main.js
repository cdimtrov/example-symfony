$(function () {
    $.support.transition = true; // enable bootstrap transitions
    $(".switchButton").bootstrapSwitch();



    // slider sortable
    $("#sortable-slider").sortable({
        placeholder: "ui-state-highlight"
    });
    $("#sortable-slider").disableSelection();

   

    // slider sortable end
});
