<?php
/**
 * Created by PhpStorm.
 * User: chavdar
 * Date: 05.02.16
 * Time: 18:59
 */

namespace AppBundle\Traits;

trait SluggableMethods
{
    private function generateSlugValue($values)
    {
        $usableValues = [];
        foreach ($values as $fieldName => $fieldValue) {
            if (!empty($fieldValue)) {
                $usableValues[] = $fieldValue;
            }
        }

        if (count($usableValues) < 1) {
            throw new \UnexpectedValueException(
                'Sluggable expects to have at least one usable (non-empty) field from the following: [ ' . implode(array_keys($values), ',') .' ]'
            );
        }

        // generate the slug itself
        $sluggableText = implode(' ', $usableValues);

        if($this->getLocale() == 'bg') {
            // Remove spaces from the beginning and from the end of the string
            $string = trim($sluggableText);

            // Lower case everything
            // using mb_strtolower() function is important for non-Latin UTF-8 string | more info: http://goo.gl/QL2tzK
            $string = mb_strtolower($string, "UTF-8");;

            // Make alphanumeric (removes all other characters)
            // this makes the string safe especially when used as a part of a URL
            // this keeps latin characters and arabic charactrs as well
            $string = preg_replace("/[^a-z0-9_\s-абвгдежзийклмнопрстуфкцчшщъьюя]/u", "", $string);

            // Remove multiple dashes or whitespaces
            $string = preg_replace("/[\s-]+/", " ", $string);

            // Convert whitespaces and underscore to the given separator
            $string = preg_replace("/[\s_]/", $this->getSlugDelimiter(), $string);

            return $string;
        } else {
            $transliterator = new \Knp\DoctrineBehaviors\Model\Sluggable\Transliterator;
            $sluggableText = $transliterator->transliterate($sluggableText, $this->getSlugDelimiter());

            $urlized = strtolower( trim( preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $sluggableText ), $this->getSlugDelimiter() ) );
            $urlized = preg_replace("/[\/_|+ -]+/", $this->getSlugDelimiter(), $urlized);
        }



        return $urlized;
    }
}
