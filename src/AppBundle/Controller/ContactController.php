<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Form\Type\CategoryType;

class ContactController extends Controller
{
    public function indexAction(Request $request)
    {
        return $this->render('contact/index.html.twig', array(

        ));
    }
}
