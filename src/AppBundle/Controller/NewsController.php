<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


/**
 * Description of NewsController
 *
 * @author chavdar
 */
class NewsController extends Controller {

    public function indexAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
                        'SELECT News
                        FROM AppBundle:News News
                        Order By News.id
                        '
                );

        $news = $query->getResult
        return $this->render('news/index.html.twig', array(
            'news' => $news
        ));
    }

    public function showAction($slug, Request $request) {
        $em = $this->getDoctrine()->getManager();
        $repository = $em
            ->getRepository('AppBundle:News');
        $qb = $repository->createQueryBuilder('N')
            ->select('N, T')
            ->join('N.translations', 'T')
            ->where('T.slug = :slug')
            ->setParameter('slug', $slug);

        $product = $qb->getQuery()->getSingleResult();
        return $this->render('news/show.html.twig', array(
            'product' => $product
        ));
    }

}
