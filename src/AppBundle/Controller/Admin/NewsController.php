<?php

namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\Type\NewsType;

/**
 * Description of News
 *
 * @author chavdar
 */
class NewsController extends BaseController {

    public function indexAction() {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
                'SELECT News
                 FROM AppBundle:News News
                 
                 Order By News.id   
                '
        );

        $news = $query->getResult();

        return $this->render('backend/news/index.html.twig', array(
                    'locale' => 'bg',
                    'news' => $news
        ));
    }

    public function newAction(Request $request) {



        $news = new \AppBundle\Entity\News();
        $form = $this->createForm(NewsType::class, $news);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form->getData()->getImage();
            if ($file != null) {
                $file = $file->getBinaryContent();
                $media = new \Application\Sonata\MediaBundle\Entity\Media();
                $media->setBinaryContent($file);
                $media->setContext('news'); // video related to the user
                $media->setProviderName('sonata.media.provider.image');

                $mediaManager = $this->container->get('sonata.media.manager.media');
                $mediaManager->save($media);
            }





            $em = $this->getDoctrine()->getManager();
            $em->persist($news);
//            $product->mergeNewTranslations();
            $em->flush();

            return $this->redirectToRoute('admin_news');
        }

        return $this->render('backend/news/new.html.twig', array(
                    'news' => $news,
                    'form' => $form->createView(),
        ));
    }

    public function editAction(\AppBundle\Entity\News $news, Request $request) {
        $em = $this->getDoctrine()->getManager();
        $editForm = $this->createForm(NewsType::class, $news);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em->flush();
            $this->addFlash('success', 'post.updated_successfully');

            return $this->redirectToRoute('admin_news_edit', array('id' => $news->getId()));
        }
        $mediaManager = $this->container->get('sonata.media.manager.media');
        $media = $mediaManager->findOneBy(array('id' => $news->getImage()));

        return $this->render('backend/news/new.html.twig', array(
                    'media' => $media,
                    'news' => $news,
                    'form' => $editForm->createView()
        ));
    }

    public function deleteAction(\AppBundle\Entity\News $news) {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($news);
        $entityManager->flush();
        $this->addFlash('success', 'product.deleted_successfully');
        return $this->redirectToRoute('admin_news');
    }

}
