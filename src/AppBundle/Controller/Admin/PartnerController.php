<?php

namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\Type\PartnerType;

class PartnerController extends BaseController {

    public function indexAction() {
        
    }

//    public function newAction(Request $request) {
//
//        $partner = new \AppBundle\Entity\Partner();
//        $form = $this->createForm(PartnerType::class, $partner);
//        $form->handleRequest($request);
//
//        if ($form->isSubmitted() && $form->isValid()) {
//            $em = $this->getDoctrine()->getManager();
//            $em->persist($partner);
////            $partner->mergeNewTranslations();
//            $em->flush();
//
//            return $this->redirectToRoute('admin_about');
//        }
//
//        return $this->render('backend/partner/new.html.twig', array(
//                    'partner' => $partner,
//                    'form' => $form->createView(),
//        ));
//    }

    public function editAction(\AppBundle\Entity\Partner $partner, Request $request) {
        $em = $this->getDoctrine()->getManager();
        $editForm = $this->createForm(PartnerType::class, $partner);
        $editForm->handleRequest($request);
        
         if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em->flush();
            $this->addFlash('success', 'post.updated_successfully');

            return $this->redirectToRoute('admin_partner_edit', array('id' => $partner->getId()));
        }
        
        return $this->render('backend/partner/new.html.twig', array(
                    'partner' => $partner,
                    'form' => $editForm->createView()
        ));
    }

//    public function deleteAction(\AppBundle\Entity\Partner $partner, Request $request) {
//        $entityManager = $this->getDoctrine()->getManager();
//        $entityManager->remove($partner);
//        $entityManager->flush();
//        $this->addFlash('success', 'partner.deleted_successfully');
//        return $this->redirectToRoute('admin_partner');
//    }

}
