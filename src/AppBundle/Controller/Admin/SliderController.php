<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Controller\Admin\BaseController;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\Type\SliderType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class SliderController extends BaseController {

    public function indexAction() {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
                'SELECT Slider
                 FROM AppBundle:Slider Slider
                 
                 Order By Slider.id   
                '
        );

        $sliders = $query->getResult();

        $querySortable = $em->createQuery(
                'SELECT Slider
                 FROM AppBundle:Slider Slider
                 WHERE Slider.isActive > 0
                 Order By Slider.position
                 '
        );



        $slidersSortable = $querySortable->getResult();


        return $this->render('backend/slider/index.html.twig', array(
                    'locale' => 'bg',
                    'sliders' => $sliders,
                    'slidersSortable' => $slidersSortable
        ));
    }

    public function newAction(Request $request) {
        $slider = new \AppBundle\Entity\Slider();
        $form = $this->createForm(SliderType::class, $slider);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form->getData()->getImage();
            if ($file != null) {
                $file = $file->getBinaryContent();
                $media = new \Application\Sonata\MediaBundle\Entity\Media();
                $media->setBinaryContent($file);
                $media->setContext('slider'); // video related to the user
                $media->setProviderName('sonata.media.provider.image');

                $mediaManager = $this->container->get('sonata.media.manager.media');
                $mediaManager->save($media);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($slider);
            $em->flush();

            return $this->redirectToRoute('admin_slider');
        }



        return $this->render('backend/slider/new.html.twig', array(
                    'form' => $form->createView()
        ));
    }

    public function editAction(\AppBundle\Entity\Slider $slider, Request $request) {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(SliderType::class, $slider);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($slider);
            $em->flush();

            return $this->redirectToRoute('admin_slider');
        }

        $mediaManager = $this->container->get('sonata.media.manager.media');
        $media = $mediaManager->findOneBy(array('id' => $slider->getImage()));



        return $this->render('backend/slider/new.html.twig', array(
                    'media' => $media,
                    'form' => $form->createView()
        ));
    }

    public function deleteAction() {
        
    }

    public function ajaxAction(Request $request) {
        if ($request->isXMLHttpRequest()) {
            $em = $this->getDoctrine()->getManager();

            $order = $request->get('order');
            $orders = explode(',', $order);
            foreach ($orders as $index => $id) {
                $slider = $em->getRepository('AppBundle:Slider')->find($id);
                if (!$slider) {
                    throw $this->createNotFoundException(
                            'No slider found for id ' . $id
                    );
                }

                $slider->setPosition($index);
                $em->flush();
            }
            return new JsonResponse(array('response' => 'success'));
        }

        return new Response('This is not ajax!', 400);
    }

}
