<?php

namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\Type\TaskType;
use AppBundle\Form\Type\CategoryType;

class CategoryController extends BaseController {

    //put your code here

    public function indexAction() {
//        $product = new \AppBundle\Entity\ProductTranslation();
//        $category = new \AppBundle\Entity\CategoryTranslation();
//        var_dump($category); 
//        var_dump($product); exit();
//        $category = new \AppBundle\Entity\Category();
//        $category->translate('bg')->setName('Въх въх');
//        $category->setParent(1);
//        $category->translate('bg')->setDescription('Майкко ши го скъсам');
//        $category->translate('en')->setName('Shoes');
//        $category->translate('en')->setDescription('Description');
//        $em = $this->getDoctrine()->getManager();
//
//        $em->persist($category);
//         $category->mergeNewTranslations();
//        $em->flush();

//        $repository = $this->getDoctrine()
//                ->getRepository('AppBundle:Category');
//
//        $categories = $repository->findAll();


        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
                        'SELECT Category
                        FROM AppBundle:Category Category
                        JOIN Category.translations CatTranslation
                        WHERE CatTranslation.locale = :locale
                        AND Category.parent IS NULL
                        Order By Category.id
                        '
                )->setParameter('locale', 'bg');

//        var_dump($query);
//        exit();
        
        
        $categories = $query->getResult();

        $locale = $this->container->getParameter('default_locale');

        return $this->render('backend/category/index.html.twig', array(
                    'locale' => 'bg',
                    'categories' => $categories
        ));
    }

    public function newAction(Request $request) {

        $category = new \AppBundle\Entity\Category();
        $form = $this->createForm(CategoryType::class, $category);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form->getData()->getImage();
//             ... perform some action, such as saving the task to the database
            if ($file != null) {
                $file = $file->getBinaryContent();
                $media = new \Application\Sonata\MediaBundle\Entity\Media();
                $media->setBinaryContent($file);
                $media->setContext('category'); // video related to the user
                $media->setProviderName('sonata.media.provider.image');

                $mediaManager = $this->container->get('sonata.media.manager.media');
                $mediaManager->save($media);
            }
           

//
//            $category->translate('fr')->setName('Chaussures');
//            $category->translate('en')->setName('Shoes');
//            $em->persist($category);
            // In order to persist new translations, call mergeNewTranslations method, before flush
//            
//            exit();
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $category->mergeNewTranslations();
            $em->flush();

            return $this->redirectToRoute('admin_category');
        }


        return $this->render('backend/category/new.html.twig', array(
                    'category' => $category,
                    'form' => $form->createView(),
        ));
    }

    public function editAction(\AppBundle\Entity\Category $category, Request $request) {
//        var_dump($category);

        $entityManager = $this->getDoctrine()->getManager();
        $editForm = $this->createForm(CategoryType::class, $category);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {


//            $post->setSlug($this->get('slugger')->slugify($post->getTitle()));
//            $
//            $entityManager->persist($category);
            $entityManager->flush();
            $this->addFlash('success', 'post.updated_successfully');

            return $this->redirectToRoute('admin_category_edit', array('id' => $category->getId()));
        }
        
        $mediaManager = $this->container->get('sonata.media.manager.media');
        $media = $mediaManager->findOneBy(array('id' => $category->getImage()));

        return $this->render('backend/category/new.html.twig', array(
                    'media' => $media,
                    'category' => $category,
                    'form' => $editForm->createView()
        ));
    }

    public function newActionTEST(Request $request) {
        // create a task and give it some dummy data for this example
        $task = new \AppBundle\Entity\Task();
        $task->setTask('Write a blog post');
        $task->setDueDate(new \DateTime('tomorrow'));

//        $form = $this->createFormBuilder($task)
//                ->add('task', TextType::class)
//                ->add('dueDate', DateType::class)
//                ->add('save', SubmitType::class, array('label' => 'Create Task'))
//                ->getForm();

        $form = $this->createForm(TaskType::class, $task);


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
//             ... perform some action, such as saving the task to the database

            $em = $this->getDoctrine()->getManager();
            $em->persist($task);
            $em->flush();

            return $this->redirectToRoute('task_success');
        }

        return $this->render('backend/category/new.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    public function EXAMPLEnewAction(Request $request) {
        // create a task and give it some dummy data for this example
        $task = new \AppBundle\Entity\Task();
        $task->setTask('Write a blog post');
        $task->setDueDate(new \DateTime('tomorrow'));

//        $form = $this->createFormBuilder($task)
//                ->add('task', TextType::class)
//                ->add('dueDate', DateType::class)
//                ->add('save', SubmitType::class, array('label' => 'Create Task'))
//                ->getForm();

        $form = $this->createForm(TaskType::class, $task);


        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // ... perform some action, such as saving the task to the database

            return $this->redirectToRoute('task_success');
        }

        return $this->render('backend/category/new.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    public function deleteAction(\AppBundle\Entity\Category $category, Request $request) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($category);
            $entityManager->flush();
            $this->addFlash('success', 'category.deleted_successfully');
        return $this->redirectToRoute('admin_category');
    }

}
