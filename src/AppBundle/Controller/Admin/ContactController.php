<?php

namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;

/**
 * Description of ContactController
 *
 * @author chavdar
 */
class ContactController extends BaseController {

    public function indexAction() {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
                'SELECT Contact
                 FROM AppBundle:Contact Contact
                 
                 Order By Contact.id   
                '
        );

        $contact = $query->getResult();

        return $this->render('backend/contact/index.html.twig', array(
                    'contacts' => $contact
        ));
    }

    public function showAction() {
        
    }

}
