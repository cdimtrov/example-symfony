<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class IndexController extends BaseController {

    public function indexAction(Request $request)
    {
//        echo 'this is the admin panel';
//        var_dump($this->container->getParameter('default_locale'));
        // replace this example code with whatever you need
        return $this->render('backend/dashboard/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
        ));
    }
}
