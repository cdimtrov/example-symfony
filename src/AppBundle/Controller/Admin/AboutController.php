<?php

namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\Type\AboutType;

class AboutController extends BaseController {

    public function indexAction() {
        
    }


    public function editAction(\AppBundle\Entity\About $about, Request $request) {
        $em = $this->getDoctrine()->getManager();
        $editForm = $this->createForm(AboutType::class, $about);
        $editForm->handleRequest($request);
        
         if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em->flush();
            $this->addFlash('success', 'post.updated_successfully');

            return $this->redirectToRoute('admin_about_edit', array('id' => $about->getId()));
        }
        
        return $this->render('backend/about/new.html.twig', array(
                    'about' => $about,
                    'form' => $editForm->createView()
        ));
    }

//    public function deleteAction(\AppBundle\Entity\Product $product, Request $request) {
//        $entityManager = $this->getDoctrine()->getManager();
//        $entityManager->remove($product);
//        $entityManager->flush();
//        $this->addFlash('success', 'product.deleted_successfully');
//        return $this->redirectToRoute('admin_product');
//    }

}
