<?php

namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\Type\ProductType;

class ProductController extends BaseController {

    public function indexAction() {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
                'SELECT Product
                 FROM AppBundle:Product Product
                 
                 Order By Product.id   
                '
        );

        $products = $query->getResult();
        
        return $this->render('backend/product/index.html.twig', array(
                    'locale' => 'en',
                    'products' => $products
        ));
    }

    public function newAction(Request $request) {

        $product = new \AppBundle\Entity\Product();
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form->getData()->getImage();
//             ... perform some action, such as saving the task to the database
            if ($file != null) {
                $file = $file->getBinaryContent();
                $media = new \Application\Sonata\MediaBundle\Entity\Media();
                $media->setBinaryContent($file);
                $media->setContext('product'); // video related to the user
                $media->setProviderName('sonata.media.provider.image');

                $mediaManager = $this->container->get('sonata.media.manager.media');
                $mediaManager->save($media);
            }
            
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
//            $product->mergeNewTranslations();
            $em->flush();

            return $this->redirectToRoute('admin_product');
        }

        return $this->render('backend/product/new.html.twig', array(
                    'product' => $product,
                    'form' => $form->createView(),
        ));
    }

    public function editAction(\AppBundle\Entity\Product $product, Request $request) {
        $em = $this->getDoctrine()->getManager();
        $editForm = $this->createForm(ProductType::class, $product);
        $editForm->handleRequest($request);
        
         if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em->flush();
            $this->addFlash('success', 'post.updated_successfully');

            return $this->redirectToRoute('admin_product_edit', array('id' => $product->getId()));
        }
        
        $mediaManager = $this->container->get('sonata.media.manager.media');
        $media = $mediaManager->findOneBy(array('id' => $product->getImage()));
        
        return $this->render('backend/product/new.html.twig', array(
                    'media' => $media,
                    'product' => $product,
                    'form' => $editForm->createView()
        ));
    }

    public function deleteAction(\AppBundle\Entity\Product $product, Request $request) {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($product);
        $entityManager->flush();
        $this->addFlash('success', 'product.deleted_successfully');
        return $this->redirectToRoute('admin_product');
    }

}
