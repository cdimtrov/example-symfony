<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\Type\CategoryType;

class AboutController extends Controller {

    public function indexAction(Request $request) {

        $about = $this->getDoctrine()
                ->getRepository('AppBundle:About')
                ->find(1);

        if (!$about) {
            throw $this->createNotFoundException(
                    'No about page found '
            );
        }
        
        // replace this example code with whatever you need
        return $this->render('about/index.html.twig', array(
            'about' => $about
        ));
    }

}
