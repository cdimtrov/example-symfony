<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of PartnerController
 *
 * @author chavdar
 */
class PartnerController extends Controller {

    public function indexAction(Request $request) {

        $partner = $this->getDoctrine()
                ->getRepository('AppBundle:Partner')
                ->find(1);

        if (!$partner) {
            throw $this->createNotFoundException(
                    'No partner page found '
            );
        }

        return $this->render('partner/index.html.twig', array(
                    'partner' => $partner
        ));
    }

}
