<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of ProductController
 *
 * @author chavdar
 */
class ProductController extends Controller {

    public function indexAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
                        'SELECT Category
                        FROM AppBundle:Category Category

                        WHERE Category.parent IS NULL
                        Order By Category.id
                        '
                );

        $categories = $query->getResult();


        return $this->render('product/index.html.twig', array(
            'categories' => $categories
        ));
    }

    public function showByCategoryAction($category, Request $request) {
        $em = $this->getDoctrine()->getManager();
        $repository = $em
            ->getRepository('AppBundle:Category');
        $qb = $repository->createQueryBuilder('C')
            ->select('C, T')
            ->join('C.translations', 'T')
            ->where('T.slug = :slug')
            ->setParameter('slug', $category);

        $category =  $qb->getQuery()->getSingleResult();

        if(count($category->getChildren()) > 0) {
            dump($category->getChildren());

            $categories = $category->getChildren();
            return $this->render('product/listChildren.html.twig', array(
                'categories' => $categories
            ));
        }

        $products = $em->getRepository('AppBundle:Product')->findBy(array('category' => $category));

        return $this->render('product/list.html.twig', array(
            'category' => $category,
            'products' => $products
        ));

    }

    public function showProductBySlugAction($category, $slug, Request $request) {
        $em = $this->getDoctrine()->getManager();

        $repository = $em
            ->getRepository('AppBundle:Product');
        $qb = $repository->createQueryBuilder('P')
            ->select('P, T')
            ->join('P.translations', 'T')
            ->where('T.slug = :slug')
            ->setParameter('slug', $slug);

        $product = $qb->getQuery()->getSingleResult();
        return $this->render('product/show.html.twig', array(
            'product' => $product
        ));
    }

}
