<?php

/**
 * Created by PhpStorm.
 * User: chavdar
 * Date: 05.02.16
 * Time: 20:00
 */

namespace AppBundle\EventListener;


use Doctrine\ORM\Event\LifecycleEventArgs;
use AppBundle\Entity\Category;
use Doctrine\ORM\Event\PostFlushEventArgs;
class SlugFix
{

    public $entity;
    protected $things = [];
    public function postPersist(LifecycleEventArgs $args)
    {

//        $entity = $args->getEntity();
//        var_dump($entity->getSlug());
//        exit();
//        // only act on some "Product" entity
////        if (!$entity instanceof Category) {
////            return;
////        }
//
//        $entityManager = $args->getEntityManager();
//
//        var_dump('oxaaa');
//        exit();

        // ... do something with the Product
    }

    public function postUpdate(LifecycleEventArgs $args) {
        $em = $args->getEntityManager();
        $entity = $args->getEntity();
        $this->entity = $entity;
        $slug = $this->generateSlug($entity);
        $entity->setSlug($slug);
        $this->things[] = $entity;
    }

    public function postFlush(PostFlushEventArgs $event)
    {
        if(!empty($this->things)) {

            $em = $event->getEntityManager();

            foreach ($this->things as $thing) {
                $em->persist($thing);
            }

            $this->things = [];
            $em->flush();

        }
    }

    public function generateSlug($entity)
    {
            $fields = $entity->getSluggableFields();
            $values = [];

            foreach ($fields as $field) {
                    $methodName = 'get' . ucfirst($field);
                    if (method_exists($entity, $methodName)) {
                        $val = $entity->{$methodName}();
                    } else {
                        $val = null;
                    }

                $values[] = $val;
            }


            return $this->generateSlugValue($values);
    }


    public function generateSlugValue($values)
    {
        $usableValues = [];
        foreach ($values as $fieldName => $fieldValue) {
            if (!empty($fieldValue)) {
                $usableValues[] = $fieldValue;
            }
        }

        if (count($usableValues) < 1) {
            throw new \UnexpectedValueException(
                'Sluggable expects to have at least one usable (non-empty) field from the following: [ ' . implode(array_keys($values), ',') .' ]'
            );
        }

        // generate the slug itself
        $sluggableText = implode(' ', $usableValues);

        if($this->entity->getLocale() == 'bg') {
            // Remove spaces from the beginning and from the end of the string
            $string = trim($sluggableText);

            // Lower case everything
            // using mb_strtolower() function is important for non-Latin UTF-8 string | more info: http://goo.gl/QL2tzK
            $string = mb_strtolower($string, "UTF-8");;

            // Make alphanumeric (removes all other characters)
            // this makes the string safe especially when used as a part of a URL
            // this keeps latin characters and arabic charactrs as well
            $string = preg_replace("/[^a-z0-9_\s-абвгдежзийклмнопрстуфкцчшщъьюя]/u", "", $string);

            // Remove multiple dashes or whitespaces
            $string = preg_replace("/[\s-]+/", " ", $string);

            // Convert whitespaces and underscore to the given separator
            $string = preg_replace("/[\s_]/", $this->getSlugDelimiter(), $string);

            return $string;
        } else {
            $transliterator = new \Knp\DoctrineBehaviors\Model\Sluggable\Transliterator;
            $sluggableText = $transliterator->transliterate($sluggableText, $this->getSlugDelimiter());

            $urlized = strtolower( trim( preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $sluggableText ), $this->getSlugDelimiter() ) );
            $urlized = preg_replace("/[\/_|+ -]+/", $this->getSlugDelimiter(), $urlized);
        }



        return $urlized;
    }

    private function getSlugDelimiter()
    {
        return '-';
    }
}
