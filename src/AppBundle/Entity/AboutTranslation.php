<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="about_i18n")
 */
class AboutTranslation {

    use ORMBehaviors\Translatable\Translation;

    /**
     * @ORM\Column(type="text", length=3000)
     * @Assert\NotBlank()
     */
    public $description;
    
    public function getDescription() {
        return $this->description;
    }

    public function setDescription($description) {
        $this->description = $description;
    }




}
